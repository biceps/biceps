
function stop() {
    local ids="$@"

    for id in $ids ; do
	virsh destroy ceph$id
	virsh undefine ceph$id
	rm -f ceph$id.qcow2
    done
    virsh net-destroy ceph
    virsh net-undefine ceph
    rm -f ceph*.img
}

function start() {
    local ids="$@"

    if ! test -f debian-10.qcow2 ; then
	sudo virt-builder debian-10 --output debian-10.qcow2 --size 10G --format qcow2 --install sudo --run-command 'dpkg-reconfigure --frontend=noninteractive openssh-server' --run-command 'useradd -s /bin/bash -m debian || true ; echo "debian ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/90-debian'  --ssh-inject debian:file:$HOME/.ssh/id_rsa.pub --edit '/etc/network/interfaces: s/ens2/enp1s0/'
    fi

    if ! virsh net-list --name | grep ceph ; then
	cat > /tmp/ceph-net.xml <<EOF
            <network>
              <name>ceph</name>
              <forward mode='nat'/>
              <bridge name='virbrceph' stp='on' delay='0'/>
              <ip address='10.11.12.1' netmask='255.255.255.0'>
                <dhcp>
                  <range start='10.11.12.100' end='10.11.12.200'/>
		  <host mac='52:54:00:00:00:01' name='ceph1' ip='10.11.12.211'/>
		  <host mac='52:54:00:00:00:02' name='ceph2' ip='10.11.12.212'/>
		  <host mac='52:54:00:00:00:03' name='ceph3' ip='10.11.12.213'/>
		  <host mac='52:54:00:00:00:04' name='ceph4' ip='10.11.12.214'/>
		  <host mac='52:54:00:00:00:05' name='ceph5' ip='10.11.12.215'/>
		  <host mac='52:54:00:00:00:06' name='ceph6' ip='10.11.12.216'/>
		  <host mac='52:54:00:00:00:07' name='ceph7' ip='10.11.12.217'/>
		  <host mac='52:54:00:00:00:08' name='ceph8' ip='10.11.12.218'/>
		  <host mac='52:54:00:00:00:09' name='ceph9' ip='10.11.12.219'/>
                </dhcp>
              </ip>
            </network>
EOF
	virsh net-define /tmp/ceph-net.xml
	virsh net-start ceph
    fi


    for id in $ids ; do
	virsh destroy ceph$id
	virsh undefine ceph$id
	rm -f ceph$id.qcow2
	cp --sparse=always debian-10.qcow2 ceph$id.qcow2
	sudo virt-sysprep -a ceph$id.qcow2 --enable customize --hostname ceph$id
	virt-install --network network=ceph,mac=52:54:00:00:00:0$id --boot hd --name ceph$id --memory 2048 --vcpus 1 --cpu host --disk path=$(pwd)/ceph$id.qcow2,bus=virtio,format=qcow2 --os-type=linux --os-variant=debian10 --graphics none --noautoconsole
	case $id in
	    1)
		;;
	    2|3)
		virsh detach-device ceph$id rng.xml --live
		for drive in b c ; do
		    #
		    # Without the sleep it fails with:
		    #
		    # error: Failed to attach disk
		    # error: internal error: No more available PCI slots
		    #
		    sleep 10
		    rm -f disk$id$drive.img
		    qemu-img create -f raw disk$id$drive.img 20G
		    sudo chown libvirt-qemu disk$id$drive.img
		    virsh attach-disk ceph$id --source $(pwd)/disk$id$drive.img --target vd$drive --persistent
		done
		;;
	    *) 
		rm -f disk$id.img
		qemu-img create -f raw disk$id.img 20G
		sudo chown libvirt-qemu disk$id.img
		virsh attach-disk ceph$id --source $(pwd)/disk$id.img --target vdb --persistent
		;;
	esac
    done
}

function restart() {
    local ids="$@"
    stop $ids
    start $ids
}

if test "$1" ; then
    "$@"
else
    restart 1 2 3 5 4 6 7 8 9
fi
