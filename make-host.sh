: ${NODES:=$OAR_FILE_NODES}

mon=$(sort -u < $NODES | head -1)

cat > inventory/hosts.yml <<EOF
---
all:
  hosts:
    ceph1: {ansible_host: $mon, ansible_port: '22', ansible_user: root, ansible_python_interpreter: '/usr/bin/python3'}
EOF

i=2
sort -u < $NODES | grep yeti | while read host ; do
    echo "    ceph$i: {ansible_host: $host, ansible_port: '22', ansible_user: root, ansible_python_interpreter: '/usr/bin/python3'}"  >> inventory/hosts.yml
    i=$(expr $i + 1)
done
    
cat > inventory/osd.yml <<EOF
---
osd:
  hosts:
EOF

i=4
sort -u < $NODES | grep -v $mon | grep -v yeti | while read host ; do
    echo "    ceph$i: {ansible_host: $host, ansible_port: '22', ansible_user: root, ansible_python_interpreter: '/usr/bin/python3'}"  >> inventory/hosts.yml
    echo "    ceph$i:" >> inventory/osd.yml
    
    i=$(expr $i + 1)
done
