# Benchmark reports

* https://hedgedoc.softwareheritage.org/NMh_kgmlR6eWdR2QpgcQfA#

# Prepare the environment

eval "$(direnv hook bash)"
echo layout python3 > .envrc
direnv allow .
pip install poetry
poetry install --no-dev
pre-commit install

# Introduction

## libvirt

./build-vms.sh
runner=debian@10.11.12.211
idx=debian@10.11.12.213
eval "$(direnv hook bash)"

## grid5000

To visualize the machines available over the next week-end(s) https://intranet.grid5000.fr/oar/Grenoble/drawgantt-svg/

ssh -t ldachary@access.grid5000.fr ssh -t grenoble tmux attach
git clone https://git.easter-eggs.org/biceps/biceps

### As soon as possible

oarsub -t exotic -I -l "{cluster='dahu'}/host=7+{cluster='yeti'}/host=1,walltime=1" -t deploy

### At a given date, in advance

oarsub -t exotic -l "{cluster='dahu'}/host=7+{cluster='yeti'}/host=1,walltime=1" --reservation '2021-04-07 11:00:00' -t deploy

Keep the OAR_JOB_ID in a variable and when the cluster is available:

oarsub -C $OAR_JOB_ID

### Check network

ssh yeti-1 iperf -s
ssh dahu-29 iperf -c yeti-1 -p 5001

### Install the OS

eval "$(direnv hook bash)"
source install-grid5000.sh

If things go wrong, free all hosts immediately with:

oardel $OAR_JOB_ID

If more time is needed, the following adds one hour:

oarwalltime $OAR_JOB_ID +1

### Collect and display the results

ansible-playbook -i inventory -i grid5000 test-collect.yml
scp ldachary@access.grid5000.fr:grenoble/biceps/*.csv . ; python stats.py stats.csv too_long.csv

## install the environment

ansible-galaxy install geerlingguy.docker
ansible-playbook -i inventory ceph.yml osd.yml rw.yml tests.yml

## run tests

ansible-playbook -i inventory tests-run.yml && ssh -t $runner direnv exec bench pytest bench/test_bench.py
ansible-playbook -i inventory tests-run.yml && ssh -t $idx direnv exec bench pytest bench/test_generate.py

## run benchmark

Note that the Write Storage (i.e. where the PostgreSQL data is stored) requires may use up to 20% more space than necessary. For instance if storing 2TB, care must be taken that there is at least 2.4TB free space. See https://forge.softwareheritage.org/T3149#64822 for more information.

### Very small to verify all works

./run-bench.sh --file-count-ro 5 --rw-workers 2 --ro-workers 2 --file-size 1024 --no-warmup

### Only writing to the Write Storage

./run-bench.sh --file-count-ro 50 --rw-workers 10 --ro-workers 0 --file-size 1024 --fake-ro

### Real benchmark

#### 1TB Read Storage, 100 10GB Shards

./run-bench.sh --file-count-ro 100 --rw-workers 10 --ro-workers 5 --file-size $((10 * 1024)) --no-warmup

#### 1TB Read Storage, 25 40GB Shards

./run-bench.sh --file-count-ro 0 --rw-workers 10 --ro-workers 5 --file-size $((40 * 1024)) --no-warmup

# Benchmark run August 6th, 2021 https://git.easter-eggs.org/biceps/biceps/-/tags/v2021-08-06

Hardware: 32 dahu, 2 yeti

## benchmark

### 10 billions records global index

See https://forge.softwareheritage.org/T3422 for the results.

    $ ansible-playbook -i inventory tests-run.yml && ssh -t $idx direnv exec bench python bench/generate.py --count $((10 * 1024 * 1024 * 1024)) --index

### 100GB Shards, 35TB Read Storage

    $ ./run-bench.sh --writer-io 85 --reader-io 150 --file-count-ro 350 --rw-workers 10 --ro-workers 5 --file-size $((100 * 1024))

# Benchmark run July 16th, 2021 https://git.easter-eggs.org/biceps/biceps/-/tags/v2021-07-16

Hardware: 32 dahu, 2 yeti

## benchmark

### 10 billions records global index

See https://forge.softwareheritage.org/T3421 for the results.

    $ ansible-playbook -i inventory tests-run.yml && ssh -t $idx direnv exec bench python bench/generate.py --count $((10 * 1024 * 1024 * 1024)) --index

### 100GB Shards, 20TB Read Storage

    $ ./run-bench.sh --writer-io 85 --reader-io 150 --file-count-ro 200 --rw-workers 10 --ro-workers 5 --file-size $((100 * 1024))

# Benchmark run June 25th, 2021 https://git.easter-eggs.org/biceps/biceps/-/tags/v2021-06-25

Hardware: 30 dahu, 2 yeti

## benchmark

### 100GB Shards, 35TB Read Storage

    $ ./run-bench.sh --writer-io 85 --reader-io 150 --file-count-ro 350 --rw-workers 10 --ro-workers 5 --file-size $((100 * 1024))
    $ python stats.py stats.csv too_long.csv
    Bytes write   108.7 MB/s
    Objects write 5.3 Kobject/s
    Bytes read    103.4 MB/s
    Objects read  25.3 Kobject/s
    494961 random reads take longer than 100ms (1.7177595208163046%)

# Benchmark run June 18th, 2021 https://git.easter-eggs.org/biceps/biceps/-/tags/v2021-06-18

Hardware: 30 dahu, 4 yeti

## benchmark

### 6 billions records global index

    $ generate.py --count $((6 * 1024 * 1024 * 1024))

### 100GB Shards, 35TB Read Storage

    $ ./run-bench.sh --file-count-ro 350 --rw-workers 10 --ro-workers 5 --file-size $((100 * 1024)) --no-warmup
    ...
    WARNING:root:Objects write 6.4K/s
    WARNING:root:Bytes write 131.3MB/s
    WARNING:root:Objects read 24.3K/s
    WARNING:root:Bytes read 99.4MB/s
    WARNING:root:2.0859388857985817% of random reads took longer than 100.0ms
    WARNING:root:Worst times to first byte on random reads (ms) [10751, 8217, 7655, 7446, 7366, 6919, 6722, 6515, 6481, 6079, 5918, 5839, 5823, 5759, 5634, 5573, 5492, 5335, 5114, 5105, 5009, 4976, 4963, 4914, 4913, 4854, 4822, 4668, 4658, 4605, 4593, 4551, 4537, 4489, 4470, 4431, 4418, 4411, 4385, 4327, 4298, 4224, 4090, 4082, 4070, 4010, 3868, 3865, 3819, 3818, 3815, 3805, 3798, 3755, 3719, 3716, 3711, 3704, 3688, 3612, 3608, 3606, 3579, 3543, 3537, 3527, 3493, 3450, 3441, 3356, 3346, 3338, 3319, 3313, 3294, 3272, 3264, 3258, 3244, 3183, 3179, 3160, 3145, 3136, 3127, 3123, 3119, 3107, 3098, 3093, 3090, 3083, 3082, 3068, 3057, 3052, 3029, 3028, 3022, 3022]

# Benchmark run June 5th, 2021 https://git.easter-eggs.org/biceps/biceps/-/tags/v2021-06-05

Hardware: 29 dahu, 2 yeti

## benchmark

### 40GB Shards, 16TB Read Storage

    $ ./run-bench.sh --file-count-ro 400 --rw-workers 40 --ro-workers 20 --file-size $((40 * 1024)) --no-warmup
    ...
    WARNING:root:Objects write 5.4K/s
    WARNING:root:Bytes write 110.6MB/s
    WARNING:root:Objects read 40.7K/s
    WARNING:root:Bytes read 166.2MB/s
    WARNING:root:Time to first byte 62ms

## rbd bench on a 100GB image

    $ rbd --pool ro create --size $((100 * 1024)) t100G

### Writing: sequential 4KB

The second time around is faster. But when writing to the Read Storage only the first time performances matter:
the image is never rewritten.

    $ rbd --pool ro rm t100G
    $ rbd --pool ro create --size $((100 * 1024)) t100G
    $ rbd --pool ro bench --io-size 4096 --io-type write --io-pattern seq --io-threads 32 --io-total 100G t100G
    elapsed: 1203   ops: 26214400   ops/sec: 21776   bytes/sec: 85 MiB/s
    $ rbd --pool ro bench --io-size 4096 --io-type write --io-pattern seq --io-threads 32 --io-total 100G t100G
    elapsed: 669   ops: 26214400   ops/sec: 39141   bytes/sec: 153 MiB/s

### Writing: sequential 4MB on multiple images

    $ for id in a b c d e f ; do
	rbd --pool ro create --size 102400 ${id}100G
	rbd --pool ro bench --io-size 4M --io-type write --io-pattern seq --io-threads 32 --io-total 100G ${id}100G >& $id.out &
      done
    $ tail -n 1 *.out
    elapsed: 1580   ops: 25600   ops/sec: 16.1983   bytes/sec: 65 MiB/s
    elapsed: 864    ops: 25600   ops/sec: 29.6272   bytes/sec: 119 MiB/s
    elapsed: 1581   ops: 25600   ops/sec: 16.1902   bytes/sec: 65 MiB/s
    elapsed: 1419   ops: 25600   ops/sec: 18.039    bytes/sec: 72 MiB/s
    elapsed: 1107   ops: 25600   ops/sec: 23.105    bytes/sec: 92 MiB/s
    elapsed: 1477   ops: 25600   ops/sec: 17.3283   bytes/sec: 69 MiB/s

### Read: bulk sequential 4MB

Must be done on an image that contains data (as a result of the write bench above)
and not an empty image that was just created.

    $ rbd --pool ro bench --io-size 4096K --io-type read --io-pattern seq --io-threads 32 --io-total 100G t100G
    elapsed: 239   ops: 25600   ops/sec: 106.748   bytes/sec: 427 MiB/s

### Read: randmon 80KB

Must be done on an image that contains data (as a result of the write bench above)
and not an empty image that was just created. Note that there seems to be a lock contention somewhere
that prevents the number of ops/sec to go above ~1300 when performing random reads with rbd bench.
Reading sequentially from two readonly mounted images has little impact on the running rbd bench which
suggests it could be resolved by tweaking rbd bench. Since this problem does not prevent the benchmark
from achieving the expected performances, it was not resolved.

    $ rbd --pool ro bench --io-size 80K --io-type read --io-pattern rand --io-threads 32 --io-total 100G t100G
    elapsed: 1007   ops: 1310720   ops/sec: 1300.88   bytes/sec: 102 MiB/s

# Benchmark run May 16th, 2021 https://git.easter-eggs.org/biceps/biceps/-/tags/v2021-05-16

Hardware: 26 dahu, 2 yeti

## benchmark

### 1GB Shards, 200GB Read Storage

    $ ./run-bench.sh --file-count-ro 200 --rw-workers 40 --ro-workers 20 --file-size 1024 --no-warmup
    WARNING:root:Objects write 6.0K/s
    WARNING:root:Bytes write 122.0MB/s
    WARNING:root:Objects read 66.8K/s
    WARNING:root:Bytes read 272.7MB/s

### 10GB Shards, 2TB Read Storage

    $ ./run-bench.sh --file-count-ro 200 --rw-workers 40 --ro-workers 20 --file-size 10240 --no-warmup
    WARNING:root:Objects write 5.9K/s
    WARNING:root:Bytes write 121.3MB/s
    WARNING:root:Objects read 42.7K/s
    WARNING:root:Bytes read 174.2MB/s

### 40GB Shards, 8TB Read Storage

    $ ./run-bench.sh --file-count-ro 200 --rw-workers 40 --ro-workers 20 --file-size 40000 --no-warmup
    WARNING:root:Objects write 5.8K/s
    WARNING:root:Bytes write 118.3MB/s
    WARNING:root:Objects read 34.5K/s
    WARNING:root:Bytes read 140.8MB/s

## rbd bench on a 100GB image

    $ rbd --pool ro create --size 102400 t100G

### Writing: sequential 4KB

    $ rbd --pool ro bench --io-size 4096 --io-type write --io-pattern seq --io-threads 32 --io-total 10G t100G
    elapsed: 81   ops: 2621440   ops/sec: 32269.3   bytes/sec: 126 MiB/s

### Writing: sequential 4MB on multiple images

    $ for id in a b c d e f ; do
	rbd --pool ro create --size 102400 ${id}100G
	rbd --pool ro bench --io-size 4M --io-type write --io-pattern seq --io-threads 32 --io-total 10G ${id}100G >& $id.out &
      done
    $ tail -n 1 *.out
    elapsed: 76   ops: 2560   ops/sec: 33.4412   bytes/sec: 134 MiB/s
    elapsed: 76   ops: 2560   ops/sec: 33.4359   bytes/sec: 134 MiB/s
    elapsed: 76   ops: 2560   ops/sec: 33.3871   bytes/sec: 134 MiB/s
    elapsed: 157   ops: 2560   ops/sec: 16.3048   bytes/sec: 65 MiB/s
    elapsed: 140   ops: 2560   ops/sec: 18.2517   bytes/sec: 73 MiB/s
    elapsed: 122   ops: 2560   ops/sec: 20.8387   bytes/sec: 83 MiB/s

### Read: bulk sequential 4MB

    $ rbd --pool ro bench --io-size 4096K --io-type read --io-pattern seq --io-threads 32 --io-total 10G t100G
    elapsed: 26   ops: 2560   ops/sec: 97.4565   bytes/sec: 390 MiB/s

### Read: randmon 4KB

    $ rbd --pool ro bench --io-size 4096 --io-type read --io-pattern rand --io-threads 32 --io-total 10G t100G
    elapsed: 178   ops: 2621440   ops/sec: 14671.4   bytes/sec: 57 MiB/s
