#!/bin/bash

ansible-playbook -i inventory tests-run.yml
if test "$OAR_FILE_NODES" ; then
    runner=root@$(sort -u < $OAR_FILE_NODES | head -1)
    idx=root@$(grep yeti < $OAR_FILE_NODES | sort -r | head -1)
else
    runner=debian@10.11.12.211
    idx=debian@10.11.12.213
fi
ssh -t $idx direnv exec bench python bench/generate.py --count 1000
ssh -t $runner direnv exec bench python bench/bench.py "$@"
ansible-playbook -i inventory test-collect.yml

if test -z "$OAR_FILE_NODES" ; then
    next=false
    for arg in "$@" ; do
	if $next ; then
	    rw=$arg
	    break
	fi
	if test "$arg" == "--rw-workers" ; then
	    next=true
	fi
    done
    python stats.py stats.csv too_long.csv $rw
fi
