import sys

import pandas as pd
from matplotlib import pyplot as plt
from matplotlib.ticker import FormatStrFormatter


def human(size, unit):
    if size < 1024:
        return f"{int(size)} {unit}/s"
    elif size / 1024 < 1024:
        return f"{round(size/1024, 1)} K{unit}/s"
    elif size / (1024 * 1024) < 1024:
        return f"{round(size / (1024 * 1024), 1)} M{unit}/s"
    elif size / (1024 * 1024 * 1024) < 1024:
        return f"{round(size / (1024 * 1024 * 1024), 1)} G{unit}/s"


def main(stats, too_long, writers):
    df = pd.read_csv(stats)

    df = df[df["readers"] > 0]
    df = df[df["writers"] >= int(writers)]

    a = df.iloc[-1] - df.iloc[0]
    a /= a["time"]
    print(f'Bytes write   {human(a["bytes_write"], "B")}')
    print(f'Objects write {human(a["object_write_count"], "object")}')
    print(f'Bytes read    {human(a["bytes_read"], "B")}')
    print(f'Objects read  {human(a["object_read_count"], "object")}')

    too_long_count = df["too_long"].sum()
    ratio = too_long_count / df.iloc[-1]["random_object_read_count"]
    print(f"{too_long_count} random reads take longer than 100ms ({ratio*100}%)")

    df["date"] = pd.to_datetime(df["time"], unit="s")
    start_time = df.iloc[0]["time"]
    tl = pd.read_csv(too_long)
    tl = tl[tl["time"] >= start_time]
    tl["date"] = pd.to_datetime(tl["time"], unit="s")

    p = tl.plot(x="date", y="too_long")
    p.set_xlabel("Date")
    p.set_ylabel("Milliseconds")
    p.legend(["Read times over 100ms"])

    for (l, h) in (("read_bytes_sec", "Read"), ("write_bytes_sec", "Write")):
        df[h] = df[l] / (1 * 1000 * 1000)
        p = df.plot(x="date", y=[h])
        p.set_xlabel("Date")
        p.yaxis.set_major_formatter(FormatStrFormatter("%.0f"))
        p.set_ylabel("MB/s")
    plt.show()


if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2], sys.argv[3])
