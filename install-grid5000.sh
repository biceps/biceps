#kadeploy3 -f $OAR_NODE_FILE -e debian10-x64-base -k

: ${NODES:=$OAR_FILE_NODES}

bash make-host.sh
runner=root@$(sort -u < $NODES | head -1)
idx=root@$(grep yeti < $NODES | sort -r | head -1)
ansible-playbook -i inventory -i grid5000 grid5000.yml ceph.yml osd.yml rw.yml tests.yml
