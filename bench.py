#!env python
#
# .direnv/python-3.8.5/bin/flake8 --max-line-length 110 bench.py test_bench.py stats.py
#
# sudo .direnv/python-3.7.3/bin/python bench.py \
#    --file-count-ro 5 --rw-workers 2 --packer-workers 2 --file-size 1024
#
import abc
import argparse
import asyncio
import concurrent.futures
import json
import logging
import os
import random
import struct
import sys
import time
import uuid

import psycopg2
import sh


class Throttle(object):
    def __init__(self, limit):
        self.sleep_interval = 0.01
        self.limit = limit
        self.bucket = limit
        self.time = time.time
        self.sleep = time.sleep
        self.last = self.time()

    def tick(self):
        now = self.time()
        leak = (now - self.last) * self.limit
        self.bucket = max(0, self.bucket - leak)
        self.last = now

    def add(self, bytes):
        self.tick()
        if self.bucket >= self.limit:
            need = max(0, (self.bucket - self.limit) + bytes)
            delay = need / self.limit
            self.sleep(delay)
            self.tick()
        self.bucket += bytes


class Random(object):
    def __init__(self):
        self.path = "/tmp/random"

    def init(self, size):
        size += 1
        bytes = size * 1024 * 1024
        if not os.path.exists(self.path) or os.path.getsize(self.path) < bytes:
            os.system(f"dd if=/dev/urandom of={self.path} count={size} bs=1024k")


class Pool(object):
    def __init__(self, args):
        self.args = args

    def init(self):
        self.rbd = sh.sudo.bake("rbd", f"--pool={self.name}")
        self.ceph = sh.sudo.bake("ceph")
        self.image_size = self.args.file_size * 2

    def uninit(self):
        pass

    def image_list(self):
        try:
            self.rbd.ls()
        except sh.ErrorReturnCode_2 as e:
            if "No such file or directory" in e.args[0]:
                return []
            else:
                raise
        return [image.strip() for image in self.rbd.ls()]

    def image_path(self, image):
        return f"/dev/rbd/{self.name}/{image}"

    def image_create(self, image):
        logging.warning(f"rdb --pool {self.name} create --size={self.image_size} {image}")
        self.rbd.create(f"--size={self.image_size}", f"--data-pool={self.name}-data", image)
        self.rbd.feature.disable(f"{self.name}/{image}", "object-map", "fast-diff", "deep-flatten")
        self.image_map(image, "rw")

    def image_map(self, image, options):
        self.rbd.device("map", "-o", options, image)
        sh.sudo("chmod", "777", self.image_path(image))

    def image_remap_ro(self, image):
        self.image_unmap(image)
        self.image_map(image, "ro")

    def image_unmap(self, image):
        self.rbd.device.unmap(f"{self.name}/{image}", _ok_code=(0, 22))

    def image_delete(self, image):
        self.image_unmap(image)
        logging.warning(f"rdb --pool {self.name} remove {image}")
        self.rbd.remove(image)

    def images_clobber(self):
        for image in self.image_list():
            image = image.strip()
            self.image_unmap(image)

    def clobber(self):
        self.images_clobber()
        self.pool_clobber()


class StorageRO(Pool):
    name = "ro"

    def pool_clobber(self):
        logging.warning(f"ceph osd pool delete {self.name}")
        self.ceph.osd.pool.delete(self.name, self.name, "--yes-i-really-really-mean-it")
        data = f"{self.name}-data"
        logging.warning(f"ceph osd pool delete {data}")
        self.ceph.osd.pool.delete(data, data, "--yes-i-really-really-mean-it")

    def pool_create(self):
        data = f"{self.name}-data"
        logging.warning(f"ceph osd pool create {data}")
        self.ceph.osd(
            "erasure-code-profile",
            "set",
            "--force",
            data,
            "k=4",
            "m=2",
            "crush-failure-domain=host",
        )
        self.ceph.osd.pool.create(data, "200", "erasure", data)
        self.ceph.osd.pool.set(data, "allow_ec_overwrites", "true")
        self.ceph.osd.pool.set(data, "pg_autoscale_mode", "off")
        logging.warning(f"ceph osd pool create {self.name}")
        self.ceph.osd.pool.create(self.name)
        if self.args.rw_workers > 0:
            self.ceph.config.set.client(
                "rbd_qos_write_bps_limit",
                int(((self.args.writer_io * 2) * 1024 * 1024) / self.args.rw_workers),
            )
        if self.args.ro_workers > 0:
            self.ceph.config.set.client(
                "rbd_qos_read_bps_limit",
                int((self.args.reader_io * 1024 * 1024) / self.args.ro_workers),
            )


class StorageRW(object):
    def __init__(self, args):
        self.args = args
        # the rw host is the host with the postgresql server
        # see tests.yml
        self.dsn = "postgres://testuser:testpassword@rw"

    def init(self):
        Random().init(max(self.args.file_size, 1))

    def uninit(self):
        pass

    def image_list(self):
        db = psycopg2.connect(f"{self.dsn}/postgres")
        db.autocommit = True
        with db.cursor() as c:
            c.execute(
                "SELECT datname FROM pg_database " "WHERE datistemplate = false and datname != 'postgres'"
            )
            return [r[0] for r in c.fetchall()]

    def image_create(self):
        image = f"i{uuid.uuid4().hex}"
        db = psycopg2.connect(f"{self.dsn}/postgres")
        db.autocommit = True
        c = db.cursor()
        c.execute(f"CREATE DATABASE {image}")
        c.close()
        return image

    def image_path(self, image):
        return f"{self.dsn}/{image}"

    def image_delete(self, name):
        db = psycopg2.connect(f"{self.dsn}/postgres")
        # https://wiki.postgresql.org/wiki/Psycopg2_Tutorial
        # If you want to drop the database you would need to
        # change the isolation level of the database.
        db.set_isolation_level(0)
        db.autocommit = True
        c = db.cursor()
        #
        # Dropping the database may fail because the server takes time
        # to notice a connection was dropped and/or a named cursor is
        # in the process of being deleted. It can happen here or even
        # when deleting all database with the psql cli
        # and there are no bench process active.
        #
        # ERROR:  database "i606428a5a6274d1ab09eecc4d019fef7" is being accessed by other users
        # DETAIL:  There is 1 other session using the database.
        #
        # See:
        # https://stackoverflow.com/questions/5108876/kill-a-postgresql-session-connection
        #
        # https://www.postgresql.org/docs/current/sql-dropdatabase.html
        #
        # WITH (FORCE) added in postgresql 13 but may also fail because the
        # named cursor may not be handled as a client.
        #
        for i in range(60):
            try:
                c.execute(f"DROP DATABASE IF EXISTS {name}")
                break
            except psycopg2.errors.ObjectInUse:
                logging.warning(f"{name} database drop fails, waiting")
                time.sleep(10)
                continue
            raise Exception("database drop fails {name}")
        c.close()

    def clobber(self):
        for image in self.image_list():
            self.image_delete(image)


class Database(abc.ABC):
    def __init__(self, args, path):
        self.args = args
        self.path = path

    @abc.abstractmethod
    def open(self, *args, **kwargs):
        pass

    @abc.abstractmethod
    def close(self):
        pass


class DatabaseRW(Database):
    @abc.abstractmethod
    def index(self):
        pass

    @abc.abstractmethod
    def values(self):
        pass

    @abc.abstractmethod
    def fill(self):
        pass


class DatabasePostgreSQL(DatabaseRW):
    # for d in $(psql -c '\l' | grep '^ i' | cut -f2 -d ' ') ; do psql -c "drop database $d" ; done
    def __init__(self, args, path):
        super().__init__(args, path)
        self.payloads_define()
        self.limit = 1000
        self._count = None

    def open(self):
        self.idx = psycopg2.connect("postgres://testuser:testpassword@idx/globalindex")
        self.db = psycopg2.connect(self.path)
        c = self.db.cursor()
        c.execute("CREATE TABLE IF NOT EXISTS objects(key BYTEA PRIMARY KEY, content BYTEA)")
        c.close()
        self.db.commit()

    def close(self):
        self.db.close()
        self.idx.close()

    def index(self):
        # psql service=swh -c "\copy (select sha1, length from content) to stdout delimiter ','"
        with self.db.cursor(name="index") as c:
            c.execute("SELECT key,LENGTH(content) FROM objects ORDER BY key")
            for row in c:
                yield (row[0].tobytes(), row[1])

    def values(self):
        with self.db.cursor(name="values") as c:
            c.execute("SELECT content FROM objects ORDER BY key")
            for row in c:
                yield row[0].tobytes(),

    def object_count(self):
        with self.db.cursor() as c:
            c.execute("SELECT COUNT(*) FROM objects")
            return c.fetchone()[0]

    def payloads_define(self):
        self.payloads = [
            3 * 1024,
            3 * 1024,
            3 * 1024,
            3 * 1024,
            3 * 1024,
            10 * 1024,
            13 * 1024,
            16 * 1024,
            70 * 1024,
            80 * 1024,
        ]

    def fill(self):
        random_index = open(Random().path, "rb")
        random_content = open(Random().path, "rb")
        max_size = self.args.file_size * 1024 * 1024
        size = 0
        self._count = 0
        batch_size = 100
        while size < max_size:
            args = []
            idx = []
            values = []
            for _ in range(batch_size):
                index = random_index.read(32)
                payload = random_content.read((random.choice(self.payloads)))
                args.extend((index, payload))
                idx.extend((payload[:8], payload[:2]))
                values.append("(%s, %s)")
                size += len(payload)
                self._count += 1
            values = ",".join(values)
            with self.db.cursor() as c:
                c.execute(f"INSERT INTO objects (key, content) VALUES {values};", args)
            self.db.commit()
            with self.idx.cursor() as c:
                c.execute(f"INSERT INTO shards (signature, shard) VALUES {values};", idx)
            self.idx.commit()
        return self._count, size


class DatabaseRO(Database):

    version = 1

    format_header = "!IQ"
    format_index = "!32sQQ"

    def __init__(self, args, path):
        super().__init__(args, path)
        self.index_buffer = b""
        self.content_buffer = b""
        self.buffer_size = 4 * 1024 * 1024

    def open(self, mode="rb", size=0):
        self.fd = open(self.path, mode)
        if mode == "wb":
            self.fd.write(struct.pack(DatabaseRO.format_header, DatabaseRO.version, size))
            self.size = size
            self.data_offset = self.fd.tell() + struct.calcsize(DatabaseRO.format_index) * size
        else:
            self.header_size = struct.calcsize(DatabaseRO.format_header)
            header = self.fd.read(self.header_size)
            version, self.size = struct.unpack(DatabaseRO.format_header, header)
            self.index_size = struct.calcsize(DatabaseRO.format_index)
            self.data_offset = self.header_size + struct.calcsize(DatabaseRO.format_index) * self.size
            assert version == self.version

    def close(self):
        return self.fd.close()


def write_and_pack(args):
    #
    # Write
    #
    rw = StorageRW(args)
    rw.init()
    image = rw.image_create()
    logging.warning(f"write: starting {image}")
    d = DatabasePostgreSQL(args, rw.image_path(image))
    d.open()
    objects, size = d.fill()
    d.close()
    rw.uninit()
    logging.warning(f"write: finished {image}")
    #
    # Pack
    #
    logging.warning(f"pack: starting on {image}")
    packer = Packer(args)
    packer.init()
    packer.pack(image, objects)
    packer.uninit()
    logging.warning(f"pack: finished {image}")
    return image, objects, size


class Packer(object):
    def __init__(self, args):
        self.args = args
        self.DatabaseRW = DatabasePostgreSQL
        self.DatabaseRO = DatabaseRO

    def init(self):
        self.storage_ro = StorageRO(self.args)
        self.storage_ro.init()
        self.storage_rw = StorageRW(self.args)
        self.storage_rw.init()

    def uninit(self):
        self.storage_ro.uninit()
        self.storage_rw.uninit()

    def pack(self, image, objects):
        logging.warning(f"Packer.pack {image}")
        db_rw = self.DatabaseRW(self.args, self.storage_rw.image_path(image))
        db_rw.open()
        self.storage_ro.image_create(image)
        db_ro = self.DatabaseRO(self.args, self.storage_ro.image_path(image))
        db_ro.open(mode="wb", size=objects)
        # * 2 because it needs to catch up with the time spent creating the Shard
        # in the Write Storage
        throttler = Throttle(((self.args.writer_io * 2) * 1024 * 1024) / self.args.rw_workers)
        random_content = open(Random().path, "rb")
        random_limit = 4 * 1024 * 1024
        index_size = 0
        index_entry_size = struct.calcsize(DatabaseRO.format_index)
        total_size = 0
        for key, size in db_rw.index():
            if index_size > random_limit:
                logging.warning(f"Packer.pack index {image} at {total_size}")
                throttler.add(index_size)
                db_ro.fd.write(random_content.read(index_size))
                total_size += index_size
                index_size = 0
            index_size += index_entry_size

        display_limit = 1 * 1024 * 1024 * 1024
        display_size = 0
        content_size = 0
        for (content,) in db_rw.values():
            if display_size > display_limit:
                display_size = 0
                logging.warning(f"Packer.pack content {image} at {total_size}")
            if content_size > random_limit:
                throttler.add(content_size)
                db_ro.fd.write(random_content.read(content_size))
                total_size += content_size
                content_size = 0
            display_size += len(content)
            content_size += len(content)

        logging.warning(f"Packer.pack finished {image}")
        db_ro.close()
        self.storage_ro.image_remap_ro(image)
        db_rw.close()
        self.storage_rw.image_delete(image)
        logging.warning(f"Packer.pack deleted {image}")
        return objects, total_size


class Reader(object):
    def __init__(self, args, image):
        self.args = args
        self.size = self.args.file_size * 1024 * 1024
        self.bytes_read = 0
        self.objects_count = 0
        self.image = image

    def init(self):
        self.storage_ro = StorageRO(self.args)
        self.storage_ro.init()
        self.fd = open(self.storage_ro.image_path(self.image), "rb")

    def uninit(self):
        self.fd.close()
        self.storage_ro.uninit()

    def read_rand(self):
        payloads = [
            3 * 1024,
            3 * 1024,
            3 * 1024,
            3 * 1024,
            3 * 1024,
            10 * 1024,
            13 * 1024,
            16 * 1024,
            70 * 1024,
            80 * 1024,
        ]
        #
        # Assuming a read distribution identical to the object size distribution
        #
        object_size = random.choice(payloads)
        count = 1024
        too_long = []
        for _ in range(count):
            s = time.time()
            self.fd.seek(random.randrange(self.size - object_size))
            self.fd.read(object_size)
            duration = time.time() - s
            if duration > self.args.read_threshold:
                too_long.append(duration)
        return count, count * object_size, too_long

    def read_seq(self):
        object_size = 4 * 1024 * 1024
        count = 100
        bytes_read = count * object_size
        self.fd.seek(random.randrange(self.size - bytes_read))
        for _ in range(count):
            time.sleep(0.5)
            self.fd.read(object_size)
        #
        # Assuming a median object size of 4K
        #
        objects_count = bytes_read / (4 * 1024)
        return objects_count, bytes_read


def reader(args, image, pattern):
    reader = Reader(args, image)
    reader.init()
    if pattern == "rand":
        (objects_count, bytes_read, too_long) = reader.read_rand()
    else:
        too_long = None
        (objects_count, bytes_read) = reader.read_seq()
    reader.uninit()
    return (image, objects_count, bytes_read, too_long, pattern)


class Bench(object):
    def __init__(self, args):
        self.args = args
        self.stats = {
            "object_write_count": 0,
            "bytes_write": 0,
            "object_read_count": 0,
            "bytes_read": 0,
            "too_long": [],
            "random_object_read_count": 0,
        }
        self.readers = set()
        self.writers = set()
        self.rw_count = 0
        self.images = []
        self.stats_interval = 5

    def init(self):
        self.ro = StorageRO(self.args)
        self.ro.init()
        self.ro.pool_create()
        self.rw = StorageRW(self.args)
        self.rw.init()

    def uninit(self):
        self.ro.uninit()
        self.rw.uninit()

    def clobber(self):
        self.ro.clobber()
        self.rw.clobber()

    def reset(self):
        self.ro = StorageRO(self.args)
        self.ro.init()
        self.ro.clobber()
        self.rw = StorageRW(self.args)
        self.rw.init()
        self.rw.clobber()

    def update_too_long(self, too_long):
        if too_long is None:
            return False
        too_long = [round(t * 1000) for t in too_long]
        self.stats["too_long"] = sorted(self.stats["too_long"] + too_long, reverse=True)
        return True

    async def ro_loop(self, finished):
        if self.args.ro_workers <= 0:
            await finished.wait()
            return

        loop = asyncio.get_running_loop()
        with concurrent.futures.ThreadPoolExecutor(max_workers=self.args.ro_workers) as executor:

            logging.warning("Bench.ro_loop: running")
            self.readers = set()
            pending_readers = ["seq"] * (self.args.ro_workers - self.args.rand_ratio) + [
                "rand"
            ] * self.args.rand_ratio

            def create_reader(pattern):
                return loop.run_in_executor(executor, reader, self.args, random.choice(self.images), pattern)

            while not finished.is_set():

                if len(self.images) <= 0:
                    await asyncio.sleep(1)
                    continue

                for pattern in pending_readers:
                    self.readers.add(create_reader(pattern))
                pending_readers = []

                logging.debug(f"Bench.ro_loop: waiting on {len(self.readers)} readers")
                done, self.readers = await asyncio.wait(self.readers, return_when=asyncio.FIRST_COMPLETED)
                for task in done:
                    (image, objects, size, too_long, pattern) = task.result()
                    pending_readers.append(pattern)
                    logging.debug(f"Bench.ro_loop: {image} {objects} objects, {size} bytes")
                    self.stats["object_read_count"] += objects
                    self.stats["bytes_read"] += size
                    if self.update_too_long(too_long):
                        self.stats["random_object_read_count"] += objects

            logging.warning("Bench.ro_loop: finished")

    async def rw_loop(self, rw_workers, rw_count, warmup_delay):
        loop = asyncio.get_running_loop()
        with concurrent.futures.ProcessPoolExecutor(max_workers=rw_workers) as executor:

            logging.warning("Bench.rw_loop: running")

            self.rw_count = 0
            self.writers = set()

            def create_writer():
                self.rw_count += 1
                logging.warning(f"Bench.rw_loop: launched writer number {self.rw_count}")
                return loop.run_in_executor(executor, write_and_pack, self.args)

            for _ in range(rw_workers):
                self.writers.add(create_writer())
                #
                # Wait so that workers are not all in sync, to balance the workload.
                #
                await asyncio.sleep(warmup_delay)

            while len(self.writers) > 0:
                logging.warning(f"Bench.rw_loop: waiting for {len(self.writers)} writers")
                current = self.writers
                done, pending = await asyncio.wait(current, return_when=asyncio.FIRST_COMPLETED)
                self.writers = pending
                for task in done:
                    (image, objects, size) = task.result()
                    self.images.append(image)
                    self.stats["object_write_count"] += objects
                    self.stats["bytes_write"] += size
                    logging.warning(f"Bench.rw_loop: writer complete {image}")
                    if self.rw_count < rw_count:
                        self.writers.add(create_writer())

            logging.warning("Bench.rw_loop: finished")

    async def main(self):
        if self.args.no_warmup:
            warmup_delay = 0
        else:
            #
            # The "warmup delay" is the time it takes for one
            # writer+packer to complete, divided by the number of
            # workers.
            #
            logging.warning("Bench.main: calculate the warmup delay")
            begin = time.time()
            rw = asyncio.create_task(self.rw_loop(1, 1, 0))
            await rw
            duration = time.time() - begin
            logging.warning(f"Bench.main: warmup run took {duration}")
            warmup_delay = duration / self.args.rw_workers
            logging.warning(f"Bench.main: warmup delay is {warmup_delay}")

        finished = asyncio.Event()
        rw = asyncio.create_task(self.rw_loop(self.args.rw_workers, self.args.file_count_ro, warmup_delay))
        ro = asyncio.create_task(self.ro_loop(finished))
        stats = asyncio.create_task(self.stats_loop(finished))
        logging.warning("Bench.main: waiting for rw to complete")
        await rw
        finished.set()
        await ro
        await stats
        logging.warning("Bench.main: finished")
        return 0

    async def stats_loop(self, finished):
        self.stats_open()
        while not finished.is_set():
            await asyncio.sleep(self.stats_interval)
            self.stats_print()
        self.stats_close()

    async def main_ro(self, finished):
        self.stats["object_write_count"] = 1
        self.images = self.ro.image_list()
        logging.warning(f"main_ro on {len(self.images)} images")
        stats = asyncio.create_task(self.stats_loop(finished))
        ro = asyncio.create_task(self.ro_loop(finished))
        await ro
        await stats
        return 0

    def stats_open(self):
        self.too_long_fd = open(f"{self.args.tmp}/too_long.csv", "w")
        self.too_long_fd.write("time," "too_long" "\n")
        self.stats_fd = open(f"{self.args.tmp}/stats.csv", "w")
        self.stats_fd.write(
            # time in seconds since epoch
            "time,"
            # total number of writers that completed writing a Shard to the Read Storage
            "rw_count,"
            # number of writers in flight at this point in time
            "writers,"
            # number of readers in flight at this point in time
            "readers,"
            # total number of objects written at this point in time
            "object_write_count,"
            # total number of bytes written at this point in time
            "bytes_write,"
            # total number of objects read at this point in time
            "object_read_count,"
            # total number of bytes read at this point in time
            "bytes_read,"
            # total number of objects read via a random read at this point in time
            # (included in object_read_count)
            "random_object_read_count,"
            # number of bytes stored in the Ceph cluster
            "data_bytes,"
            # number of raw bytes used by the Ceph cluster
            "bytes_used,"
            # number of bytes available in the Ceph cluster
            "bytes_avail,"
            # total number of bytes in the Ceph cluster
            "bytes_total,"
            # number of bytes read per sec from the Ceph cluster as reported by the Ceph cluster
            "read_bytes_sec,"
            # number of bytes write per sec to the Ceph cluster as reported by the Ceph cluster
            "write_bytes_sec,"
            # number of write operations per sec in the Ceph cluster as reported by the Ceph cluster
            "read_op_per_sec,"
            # number of read operations per sec in the Ceph cluster as reported by the Ceph cluster
            "write_op_per_sec,"
            # number of random reads with a latency > 100ms since the last interval
            "too_long"
            "\n"
        )

    def stats_print(self):
        now = time.time()
        r = self.ro.ceph.status("--format=json")
        s = json.loads(r.stdout.decode("UTF-8"))["pgmap"]
        ll = ",".join(
            [
                str(x)
                for x in [
                    int(now),
                    self.rw_count,
                    len(self.writers),
                    len(self.readers),
                    self.stats["object_write_count"],
                    self.stats["bytes_write"],
                    self.stats["object_read_count"],
                    self.stats["bytes_read"],
                    self.stats["random_object_read_count"],
                    s.get("data_bytes", 0),
                    s.get("bytes_used", 0),
                    s.get("bytes_avail", 0),
                    s.get("bytes_total", 0),
                    s.get("read_bytes_sec", 0),
                    s.get("write_bytes_sec", 0),
                    s.get("read_op_per_sec", 0),
                    s.get("write_op_per_sec", 0),
                    len(self.stats["too_long"]),
                ]
            ]
        )
        self.stats_fd.write(f"{ll}\n")
        self.stats_fd.flush()

        if self.stats["too_long"]:
            for x in self.stats["too_long"]:
                self.too_long_fd.write(f"{now},{x}\n")
        else:
            self.too_long_fd.write(f"{now},100\n")
        self.too_long_fd.flush()
        self.stats["too_long"] = []
        return True

    def stats_close(self):
        self.too_long_fd.close()
        self.stats_fd.close()


class Application(object):
    def parse_args(self, argv):
        parser = argparse.ArgumentParser(description="bench")
        parser.add_argument("--file-count-ro", required=True, type=int)
        parser.add_argument("--ro-workers", default=1, type=int)
        parser.add_argument("--reader-io", default=100, type=float)
        parser.add_argument("--writer-io", default=100, type=float)
        parser.add_argument("--rand-ratio", default=5, type=int)
        parser.add_argument("--rw-workers", default=1, type=int)
        parser.add_argument("--file-size", required=True, type=int)
        parser.add_argument("--read-threshold", default=0.1, type=int)
        parser.add_argument("--tmp", default="/tmp")
        parser.add_argument(
            "--no-warmup",
            help="do not impose a delay on workloads when starting",
            action="store_true",
        )
        parser.add_argument(
            "--no-reset",
            help="do reset the storage at startup",
            action="store_true",
        )
        return parser.parse_args(argv)

    async def main(self, argv):
        args = self.parse_args(argv)
        bench = Bench(args)
        if not args.no_reset and args.rw_workers > 0:
            bench.reset()
        bench.init()
        if args.rw_workers > 0:
            await bench.main()
        else:
            await bench.main_ro(asyncio.Event())
        bench.uninit()


if __name__ == "__main__":
    sys.exit(asyncio.run(Application().main(sys.argv[1:])))
