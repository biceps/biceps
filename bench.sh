#!/usr/bin/bash -x
#
# for f in test_bench.py bench.py bench.sh Pipfile* ; do scp debian@10.11.12.211:bench/$f . ;  done
#
function reset() {

    # unmap all mapped images
    ceph osd pool delete ro-rbd ro-rbd --yes-i-really-really-mean-it
    ceph osd pool delete ro ro --yes-i-really-really-mean-it
    ceph osd pool create ro-rbd
    ceph osd erasure-code-profile set --force ro k=4 m=2 crush-failure-domain=host
    ceph osd pool create ro erasure ro

    ceph osd pool delete rw rw --yes-i-really-really-mean-it
    ceph osd pool create rw
}

function workload() {
    ssh debian@10.11.12.211 sudo direnv exec bench python bench.py "$@"
}

function upload_bench() {
    ssh debian@10.11.12.211 <<'EOF'
set -x
sudo apt-get install -y direnv virtualenv python3-dev tmux emacs-nox
if ! grep -q direnv .bashrc ; then
    echo 'eval "$(direnv hook bash)"' >> .bashrc
fi
mkdir -p bench
echo layout python3 > bench/.envrc
direnv allow bench
direnv exec bench pip install pipenv
EOF
    scp test_bench.py bench.py bench.sh Pipfile* debian@10.11.12.211:bench/
    ssh debian@10.11.12.211 direnv exec bench pipenv install
}

function run_local() {
    ./build-vms.sh restart
    ansible-playbook -i inventory ceph.yml rw.yml
    ansible -i inventory -a 'du -sh /var/ceph/osd' ceph
    upload_bench
    workload "$@"
    ansible -i inventory -a 'du -sh /var/ceph/osd' ceph
}

function main() {
    run_local "$@"
}

"$@"
