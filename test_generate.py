import argparse
import os
import time

import pytest

import generate


def test_create_payload(tmpdir):
    f = f"{tmpdir}/data"
    count = 20
    generate.create_payload(f, count)
    line_size = 32 + 1 + 8 + 1
    assert os.path.getsize(f) == count * line_size


def test_Database(tmpdir):
    args = argparse.Namespace()
    d = generate.Database(args)
    d.create()
    assert d.list() == [d.name]
    d.open()
    f = f"{tmpdir}/data"
    open(f, "w").write(
        "01234567890123456789012345678901\t01234567\n"
        "11234567890123456789012345678901\t11234567\n"
        "21234567890123456789012345678901\t21234567\n"
    )
    d.copy(f)
    d.create_index()
    d.drop_index()
    d.close()
    d.delete()
    assert d.list() == []


def test_copy_payload(tmpdir):
    args = argparse.Namespace()
    d = generate.Database(args)
    d.create()
    d.open()
    f = f"{tmpdir}/data"
    assert d.size() == 0
    count = 20
    generate.create_payload(f, count)
    generate.copy_payload(args, f)
    assert d.size() == count
    d.close()
    d.delete()
    assert os.path.exists(f) is False


def test_stats():
    args = argparse.Namespace()
    b = generate.Generate(args)
    now = time.time()
    duration = 60
    before = now - duration
    b.stats = {
        "begin": before,
        "row_count": 600,
    }
    b.stats_calculate(now)
    assert b.stats["rows_per_second"] == "10/s"
    assert b.stats_print(now) is True


@pytest.mark.asyncio
async def test_Generate_write_loop(tmpdir):
    args = argparse.Namespace(tmp=tmpdir)
    d = generate.Database(args)
    d.create()
    d.delete()
    d.create()
    g = generate.Generate(args)
    g.rows_per_file = 10
    count = 100
    await g.write_loop(count)
    d.open()
    assert abs(d.size() - count) <= g.rows_per_file
    d.close()
    d.delete()


@pytest.mark.asyncio
async def test_Generate_main(tmpdir):
    count = 100
    args = argparse.Namespace(tmp=tmpdir, count=count, reset=False, index=False)

    d = generate.Database(args)
    d.delete()

    g = generate.Generate(args)
    g.rows_per_file = 10
    g.stats_interval = 1

    await g.main()

    d.open()
    assert abs(d.size() - count) <= g.rows_per_file
    d.close()

    args = argparse.Namespace(tmp=tmpdir, count=count, reset=True)
    g = generate.Generate(args)
    g.rows_per_file = 10
    g.stats_interval = 1

    await g.main()

    d.open()
    assert abs(d.size() - count) <= g.rows_per_file
    d.close()
    d.delete()


@pytest.mark.asyncio
async def test_Application_index():
    args = argparse.Namespace(index=True)

    d = generate.Database(args)
    d.delete()
    d.create()

    a = generate.Application()
    await a.main(["--index", "--count=0"])

    d.open()
    d.drop_index()
    d.close()
    d.delete()
