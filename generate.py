#!env python
#
# black generate.py test_generate.py
# .direnv/python-3.8.5/bin/flake8 --max-line-length 100 generate.py test_generate.py
#
import argparse
import asyncio
import base64
import concurrent.futures
import logging
import os
import sys
import time
import uuid

import psycopg2


class Database(object):
    name = "globalindex"

    def __init__(self, args):
        self.args = args
        self.dsn = "postgres://testuser:testpassword@idx"

    def list(self):
        db = psycopg2.connect(f"{self.dsn}/postgres")
        db.autocommit = True
        with db.cursor() as c:
            c.execute(
                "SELECT datname FROM pg_database " "WHERE datistemplate = false and datname != 'postgres'"
            )
            return [r[0] for r in c.fetchall()]

    def create(self):
        if self.name in self.list():
            return False
        db = psycopg2.connect(f"{self.dsn}/postgres")
        db.autocommit = True
        c = db.cursor()
        c.execute(f"CREATE DATABASE {self.name}")
        c.close()
        return True

    def delete(self):
        db = psycopg2.connect(f"{self.dsn}/postgres")
        db.autocommit = True
        c = db.cursor()
        for i in range(60):
            try:
                c.execute(f"DROP DATABASE IF EXISTS {self.name}")
                break
            except psycopg2.errors.ObjectInUse:
                logging.warning(f"{self.name} database drop fails, waiting")
                time.sleep(10)
                continue
            raise Exception("database drop fails {self.name}")
        c.close()

    def open(self):
        self.db = psycopg2.connect(f"{self.dsn}/{self.name}")
        c = self.db.cursor()
        c.execute("CREATE TABLE IF NOT EXISTS shards(signature char(32), shard char(8))")
        c.close()
        self.db.commit()

    def size(self):
        with self.db.cursor() as c:
            c.execute("SELECT COUNT(*) FROM shards")
            return c.fetchone()[0]

    def create_index(self):
        c = self.db.cursor()
        c.execute("CREATE INDEX shards_index ON shards (signature)")
        c.close()
        self.db.commit()

    def drop_index(self):
        c = self.db.cursor()
        c.execute("DROP INDEX shards_index")
        c.close()
        self.db.commit()

    def close(self):
        self.db.close()

    def copy(self, filename):
        c = self.db.cursor()
        c.execute(f"COPY shards FROM '{filename}' (FORMAT text)")
        c.close()
        self.db.commit()


def create_payload(filename, count):
    fd = open(filename, "w")
    for _ in range(count):
        data = base64.b64encode(os.urandom(40)).decode("UTF-8")
        fd.write(f"{data[0:32]}\t{data[32:40]}\n")


def copy_payload(args, filename):
    d = Database(args)
    d.open()
    d.copy(filename)
    d.close()
    os.unlink(filename)


class Generate(object):
    def __init__(self, args):
        self.args = args
        self.stats_interval = 30
        self.stats = {
            "row_count": 0,
        }
        self.rows_per_file = 1000000

    async def write_loop(self, write_count):
        loop = asyncio.get_running_loop()
        with concurrent.futures.ProcessPoolExecutor() as executor:

            logging.warning("write_loop: running")

            copy_task = None

            while self.stats["row_count"] < write_count:
                filename = f"{self.args.tmp}/{uuid.uuid4().hex}"
                await loop.run_in_executor(executor, create_payload, filename, self.rows_per_file)
                if copy_task is not None:
                    await copy_task
                    self.stats["row_count"] += self.rows_per_file
                copy_task = loop.run_in_executor(executor, copy_payload, self.args, filename)

    async def main(self):
        d = Database(self.args)
        if self.args.reset:
            d.delete()
        d.create()
        self.stats["begin"] = time.time()
        finished = asyncio.Event()
        writer = asyncio.create_task(self.write_loop(self.args.count))
        stats = asyncio.create_task(self.stats_loop(finished))
        logging.warning("main: waiting for writer to complete")
        await writer
        finished.set()
        await stats
        return 0

    async def stats_loop(self, finished):
        while not finished.is_set():
            await asyncio.sleep(self.stats_interval)
            self.stats_print(time.time())

    def stats_calculate(self, end):
        def human(size):
            if size < 1024:
                return f"{int(size)}/s"
            elif size / 1024 < 1024:
                return f"{round(size/1024, 1)}K/s"
            elif size / (1024 * 1024) < 1024:
                return f"{round(size / (1024 * 1024), 1)}M/s"
            elif size / (1024 * 1024 * 1024) < 1024:
                return f"{round(size / (1024 * 1024 * 1024), 1)}G/s"

        seconds = end - self.stats.get("begin", time.time())
        self.stats["rows_per_second"] = human(self.stats["row_count"] / seconds)

    def stats_print(self, end):
        self.stats_calculate(end)
        logging.warning(f"Rows {self.stats['rows_per_second']}")
        return True


class Application(object):
    def parse_args(self, argv):
        parser = argparse.ArgumentParser(description="generate")
        parser.add_argument("--count", type=int)
        parser.add_argument(
            "--reset",
            help="reset the storage at startup",
            action="store_true",
        )
        parser.add_argument(
            "--index",
            help="build the index",
            action="store_true",
        )
        parser.add_argument(
            "--tmp",
            default="/tmp",
            help="path to store the temporary files",
        )
        return parser.parse_args(argv)

    async def main(self, argv):
        args = self.parse_args(argv)
        if args.count:
            await Generate(args).main()
        if args.index:
            d = Database(args)
            d.open()
            d.create_index()
            d.close()


if __name__ == "__main__":
    sys.exit(asyncio.run(Application().main(sys.argv[1:])))
