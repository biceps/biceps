#
# coverage run $(which pytest) test_bench.py ; coverage report --include bench.py --show-missing
#
import argparse
import asyncio
import csv
import logging

import pytest

import bench
import generate


@pytest.fixture
def args(tmpdir):
    return argparse.Namespace(
        file_count_ro=1,
        file_size=10,
        no_reset=False,
        no_warmup=True,
        rand_ratio=100,
        reader_io=100,
        read_threshold=0.1,
        ro_workers=1,
        rw_workers=1,
        tmp=tmpdir,
        writer_io=100,
    )


def test_reader(tmpdir, args, idx):
    s = bench.StorageRO(args)
    s.init()
    s.clobber()
    s.pool_create()

    (image, count, size) = bench.write_and_pack(args)
    (image, count, size, first_byte) = bench.reader(args, image)
    assert count > 0
    assert size > 0

    s.clobber()
    s.uninit()


@pytest.fixture
def idx():
    args = argparse.Namespace()
    d = generate.Database(args)
    d.delete()
    d.create()
    d.open()
    d.close()
    yield
    d.delete()


def test_DatabasePostegreSQL(tmpdir, idx):
    args = argparse.Namespace(file_size=0.1, tmp=tmpdir)
    s = bench.StorageRW(args)
    s.init()
    s.clobber()
    image = s.image_create()
    assert s.image_list() == [image]
    d = bench.DatabasePostgreSQL(args, s.image_path(image))
    d.open()
    d.fill()
    assert d.object_count() > 1
    for key, size in d.index():
        assert type(key) is bytes
        assert len(key) == 32
        assert size > 1024
    for (content,) in d.values():
        assert type(content) is bytes
        assert len(content) > 1024
    d.close()
    s.image_delete(image)
    s.uninit()


def test_DatabaseRO(tmpdir):
    path = f"{tmpdir}/database.ro"
    d = bench.DatabaseRO(None, path)
    size = 1
    d.open(mode="wb", size=size)
    d.close()


def test_StorageRO(args):
    name = "IMAGE"
    s = bench.StorageRO(args)
    s.init()
    s.clobber()
    s.pool_create()
    s.image_create(name)
    p = s.image_path(name)
    assert p.endswith(name)
    something = "SOMETHING"
    open(p, "w").write(something)
    assert open(p).read(len(something)) == something
    assert s.image_list() == [name]
    s.image_remap_ro(name)
    s.images_clobber()
    assert s.image_list() == [name]
    s.clobber()
    assert s.image_list() == []


def test_Packer(args, idx):
    storage_rw = bench.StorageRW(args)
    storage_rw.init()
    image = storage_rw.image_create()
    d = bench.DatabasePostgreSQL(args, storage_rw.image_path(image))
    d.open()
    d.fill()
    objects = d.object_count()
    assert objects > 1
    d.close()

    storage_ro = bench.StorageRO(args)
    storage_ro.init()
    storage_ro.clobber()
    storage_ro.pool_create()

    packer = bench.Packer(args)
    packer.init()
    packer.pack(image, objects)
    storage_rw.uninit()
    storage_ro.clobber()
    storage_ro.uninit()


def test_write_and_pack(args, idx):

    s = bench.StorageRO(args)
    s.init()
    s.clobber()
    s.pool_create()

    (image, count, size) = bench.write_and_pack(args)
    assert image in s.image_list()
    assert count > 0
    assert size > 0

    s.clobber()
    s.uninit()


@pytest.mark.asyncio
async def test_Bench_rw_loop(args, idx):

    args.file_count_ro = 2
    args.rw_workers = 2
    b = bench.Bench(args)
    b.init()

    await b.rw_loop(rw_workers=args.rw_workers, rw_count=args.file_count_ro, warmup_delay=0)
    assert len(b.images) == args.file_count_ro

    b.clobber()
    b.uninit()


@pytest.mark.parametrize("rand_ratio", [0, 100])
@pytest.mark.asyncio
async def test_Bench_ro_loop(args, rand_ratio, idx):

    args.file_count_ro = 1
    args.file_size = 128
    b = bench.Bench(args)
    b.init()

    await b.rw_loop(rw_workers=1, rw_count=args.file_count_ro, warmup_delay=0)
    assert len(b.images) == args.file_count_ro
    finished = asyncio.Event()

    async def wait_on_ro():
        while b.stats["bytes_read"] <= 0:
            logging.warning("waiting on ro_loop")
            await asyncio.sleep(1)
        finished.set()

    await asyncio.gather(b.ro_loop(finished), wait_on_ro())
    assert b.stats["bytes_read"] > 0

    b.clobber()
    b.uninit()


@pytest.mark.asyncio
async def test_Bench_main(args, idx):

    args.file_count_ro = 2
    b = bench.Bench(args)
    b.reset()
    b.init()
    await b.main()
    assert args.file_count_ro == len(b.ro.image_list())
    assert b.stats["bytes_read"] > 0
    b.uninit()


@pytest.mark.asyncio
async def test_Bench_main_ro(args, idx):

    args.file_count_ro = 2
    b = bench.Bench(args)
    b.stats_interval = 1
    b.reset()
    b.init()
    await b.main()
    assert args.file_count_ro == len(b.ro.image_list())
    assert b.stats["bytes_read"] > 0

    finished = asyncio.Event()
    b.stats["object_read_count"] = 0
    b.stats["bytes_read"] = 0
    main_ro = asyncio.create_task(b.main_ro(finished))
    while b.stats["bytes_read"] == 0:
        await asyncio.sleep(1)
    finished.set()
    await main_ro

    b.uninit()


def test_too_long():
    args = argparse.Namespace()
    b = bench.Bench(args)
    b.too_long_length = 5
    for i in range(2, 12, 2):
        b.update_too_long([i])
    assert b.stats["too_long"] == [10000, 8000, 6000, 4000, 2000]
    b.update_too_long([4.3000001])
    assert b.stats["too_long"] == [10000, 8000, 6000, 4300, 4000, 2000]
    b.update_too_long([20])
    assert b.stats["too_long"] == [20000, 10000, 8000, 6000, 4300, 4000, 2000]


def test_stats(args):
    b = bench.Bench(args)
    b.init()
    b.stats["too_long"] = [1, 2, 3]
    b.stats_open()
    b.stats_print()
    b.stats_print()
    b.stats_close()
    count = 0
    with open(f"{args.tmp}/stats.csv", newline="") as csvfile:
        reader = csv.reader(csvfile, delimiter=",")
        for row in reader:
            assert len(list(row)) == 18
            count += 1
    assert count == 3

    count = 0
    with open(f"{args.tmp}/too_long.csv", newline="") as csvfile:
        reader = csv.reader(csvfile, delimiter=",")
        for row in reader:
            assert len(list(row)) == 2
            count += 1
    assert count == 5

    assert b.stats["too_long"] == []
    b.uninit()


def test_throttle():
    limit = 100
    t = bench.Throttle(limit)

    assert t.bucket == limit
    t.last = 0
    t.time = lambda: 1.0 / 2.0
    t.tick()
    assert t.bucket == limit / 2.0

    assert t.last == 1.0 / 2.0
    t.time = lambda: 1.0
    t.tick()
    assert t.bucket == 0

    assert t.last == 1.0
    t.time = lambda: 2.0
    t.tick()
    assert t.bucket == 0

    t = bench.Throttle(limit)

    t.last = 0
    t.time = lambda: 1.0 / 2.0
    t.tick()
    assert t.bucket == limit / 2.0
    t.add(limit)
    assert t.bucket == limit + limit / 2.0

    t.sleep_called = False

    def sleep(delay):
        assert delay == 1.5
        now = t.time() + delay
        t.time = lambda: now
        t.sleep_called = True

    t.sleep = sleep
    t.add(limit)
    assert t.bucket == limit
    assert t.sleep_called
